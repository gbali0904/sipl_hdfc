package com.Survin_India_Pvt_LTD.app.myapplication.adapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Survin_India_Pvt_LTD.app.myapplication.activity.ImagesViewActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.Model.DataForSecondActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.R;
import com.Survin_India_Pvt_LTD.app.myapplication.activity.UpdateActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.activity.UploadImagesActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.formactivity.DataEntryBVRActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.formactivity.DataEntryRVRActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.formactivity.DataViewBVRActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.formactivity.DataViewRVRActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.utills.AgingActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.utills.TAGS;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private final Activity activity;
    String checktype,CurrentStatus;
    List<DataForSecondActivity.DataBean> userList;
    String cpvid;
    public static final String KEY_CPVID = "id";
    public static final String KEY_NAME= "username";
    public static final String KEY_APPID = "appid";
    public static final String KEY_CUserList = "userno";
    String passcpvid,passname,passappid;
    private String date2;
    private Date dateStart;
    private Date dateEnd;


    public static AppCompatButton bucttonforformfordata;
    public static AppCompatButton buttonforupdate;
    private int cpvidforsecoundactivicty;

    public RecyclerViewAdapter(Context ctx, List<DataForSecondActivity.DataBean> response) {
        this.context = ctx;
        this.activity = (Activity) ctx;
        userList = response;
    }
    @Override
    public int getItemCount() {
        return userList.size();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_row, parent, false);
        holder = new ItemViewHolder(v);
        Context context = parent.getContext();
      /*  int n = userList.size();
        Toast.makeText(context, "Total number of Items are:" + n, Toast.LENGTH_LONG).show();
 */       return holder;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;

        int itemCount;
        itemCount = getItemCount();
        //Toast.makeText(context, "Total number of Items are:" + n, Toast.LENGTH_LONG).show();
      //  ((SecondActivity) context).textView3.setText("Total No: " + itemCount);
        DataForSecondActivity.DataBean user = userList.get(position);


        checktype=user.getType().toString();
        String datetime=user.getLastsms().toString();

        String getbuttonvisibility = user.getButtonverified();

        bucttonforformfordata = itemHolder.buttonforform;
        buttonforupdate = itemHolder.buttonforupdate;
        if (getbuttonvisibility.equals("0") || getbuttonvisibility.equals(""))
        {
            bucttonforformfordata.setVisibility(View.VISIBLE);
            buttonforupdate.setVisibility(View.GONE);
        }
        else if(getbuttonvisibility.equals("1"))
        {
            bucttonforformfordata.setVisibility(View.GONE);
            buttonforupdate.setVisibility(View.VISIBLE);
        }
        String specific_instruction = user.getSpecific_instruction();

        //geting string for aging
        String s1 = getStringForAgingData(datetime);
        user.setfinalaging(s1);

        if(datetime.equals(""))
        {
            String  finaldate1=datetime;
            itemHolder.date.setText(finaldate1);
        }
        else {
            SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");//2017-02-06 11:54:13
            SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");
            Date date = null;
            try {
                date = format1.parse(datetime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //checking for BVR or RVR
        forCheckingBVROrRVRData(itemHolder, user);


        itemHolder.aging.setText(user.getfinalaging());
        itemHolder.date.setText(user.getDateOfInitiation());
        itemHolder.address.setText(user.getAddress());
        itemHolder.pinno.setText(user.getPinCode());
        itemHolder.landmark.setText(user.getLandmark());
        itemHolder.remark.setText(user.getREMARKS());
        itemHolder.instruction.setText(user.getSpecific_instruction());
        itemHolder.appno.setText(user.getApplicationReferenceNumber());
        itemHolder.type.setText(user.getType());
        itemHolder.name.setText(user.getApplicantName());
        itemHolder.executivecode.setText(user.getExecutiveCode());
        itemHolder.portfolio.setText(user.getPortfolio());
        itemHolder.txtProduct.setText(user.getProduct());
        String red=user.getRedo();
        String executivecode=user.getExecutiveCode();
        String instruction=user.getSpecific_instruction();
        //checking priority
        checkPriorityType(itemHolder, user, red,executivecode,instruction);
        forButtonsInList(itemHolder, user);
    }

    private String getStringForAgingData(String date1)  {
        DateFormat formatter = new SimpleDateFormat("yyy/MM/dd HH:mm:ss");
        String returnres = null;
        //  Toast.makeText(this,"this"+aging,Toast.LENGTH_LONG).show();
        if (!date1.equals("")) {
            date1 = date1.replace("-", "/");
            Log.e("thistag", "Total workday11" + "\n" + date1 + " day");

            Calendar c = Calendar.getInstance();
            date2 = formatter.format(c.getTime());
            try {
                dateStart = formatter.parse(date1);
                dateEnd = formatter.parse(date2);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            AgingActivity agingActivity = new AgingActivity();
            DateTimeFormatter formatter1 = DateTimeFormat.forPattern("yyy/MM/dd HH:mm:ss");
            DateTime start = formatter1.parseDateTime(date1);
            DateTime end = formatter1.parseDateTime(date2);
            Log.e("thistag", "Total workday" + "\n" + start + "\n" + date2 + " day");
            returnres = agingActivity.calculateWorkingPeriod(start, end);
            Log.e("thistag", "Total workday11" + "\n" + returnres + " day");
        } else {
            returnres = "Please Check ur Last Date";
        }
        return returnres;
    }


    private void forButtonsInList(final ItemViewHolder itemHolder, final DataForSecondActivity.DataBean user) {
        //for UploadImage
        itemHolder.buttonforupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UploadImagesActivity.class);
                passcpvid = String.valueOf(user.getCPVId());
                passname=user.getApplicantName().toString();
                passappid=user.getApplicationReferenceNumber().toString();
                intent.putExtra(KEY_CPVID, passcpvid);
                intent.putExtra(KEY_NAME, passname);
                intent.putExtra(KEY_APPID, passappid);
                context.startActivity(intent);
            }
        });
        //for view image
        itemHolder.viewimages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImagesViewActivity.class);
                passcpvid = String.valueOf(user.getCPVId());
                intent.putExtra(KEY_CPVID, passcpvid);
                context.startActivity(intent);
            }

        });
        // for update status
        itemHolder.buttonforcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UpdateActivity.class);
                passcpvid = String.valueOf(user.getCPVId());
                passname=user.getApplicantName().toString();
                passappid=user.getApplicationReferenceNumber().toString();
                intent.putExtra(KEY_CPVID, passcpvid);
                intent.putExtra(KEY_NAME, passname);
                intent.putExtra(KEY_APPID, passappid);
                context.startActivity(intent);

            }
        });

        itemHolder.buttonforform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bucttonforformfordata = itemHolder.buttonforform;
                buttonforupdate = itemHolder.buttonforupdate;
                cpvidforsecoundactivicty = user.getCPVId();
                String type_check = user.getType().toString();
                if (type_check.equals("BVR")) {
                    Intent intent = new Intent(context, DataEntryBVRActivity.class);
                    passcpvid = String.valueOf(user.getCPVId());
                    String type = user.getType().toString();
                    String executivecode = user.getExecutiveCode();
                    String getname = user.getApplicantName();
                    String getlandmark = user.getLandmark();
                    String getdepartment = user.getDepartment();
                    String getdesignation = user.getDesignation();
                    String getappno = user.getApplicationReferenceNumber();
                    intent.putExtra(TAGS.KEY_TYPE, type);
                    intent.putExtra(TAGS.KEY_CPVID, passcpvid);
                    intent.putExtra(TAGS.KEY_EXECUTIVECODE, executivecode);
                    intent.putExtra(TAGS.KEY_NAME, getname);
                    intent.putExtra(TAGS.KEY_LANDMARK, getlandmark);
                    intent.putExtra(TAGS.KEY_APPID, getappno);
                    intent.putExtra(TAGS.KEY_DEPARTMENT, getdepartment);
                    intent.putExtra(TAGS.KEY_DESIGNATION, getdesignation);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, DataEntryRVRActivity.class);
                    passcpvid = String.valueOf(user.getCPVId());
                    String type = user.getType().toString();
                    String executivecode = user.getExecutiveCode();
                    String getname = user.getApplicantName();
                    String getlandmark = user.getLandmark();
                    String getdepartment = user.getDepartment();
                    String getdesignation = user.getDesignation();
                    String getappno = user.getApplicationReferenceNumber();
                    intent.putExtra(TAGS.KEY_TYPE, type);
                    intent.putExtra(TAGS.KEY_CPVID, passcpvid);
                    intent.putExtra(TAGS.KEY_EXECUTIVECODE, executivecode);
                    intent.putExtra(TAGS.KEY_APPID, getappno);
                    intent.putExtra(TAGS.KEY_NAME, getname);
                    intent.putExtra(TAGS.KEY_LANDMARK, getlandmark);
                    intent.putExtra(TAGS.KEY_DEPARTMENT, getdepartment);
                    intent.putExtra(TAGS.KEY_DESIGNATION, getdesignation);
                    context.startActivity(intent);
                }
            }
        });
        itemHolder.buttonforupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type_check = user.getType().toString();
                if (type_check.equals("BVR")) {
                    Intent intent = new Intent(context, DataViewBVRActivity.class);
                    String cpvid = String.valueOf(user.getCPVId());
                    String type = user.getType().toString();
                    intent.putExtra(TAGS.KEY_CPVID, cpvid);
                    intent.putExtra(TAGS.KEY_TYPE, type);
                    context.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(context, DataViewRVRActivity.class);
                    String cpvid = String.valueOf(user.getCPVId());
                    String type = user.getType().toString();
                    intent.putExtra(TAGS.KEY_CPVID, cpvid);
                    intent.putExtra(TAGS.KEY_TYPE, type);
                    context.startActivity(intent);

                }

            }

        });

    }
    private void checkPriorityType(ItemViewHolder itemHolder, DataForSecondActivity.DataBean user, String instruction, String executivecode, String red) {

        if(red.equalsIgnoreCase("Y")||executivecode.equalsIgnoreCase("C")||executivecode.equalsIgnoreCase("R")||instruction.equalsIgnoreCase("R"))
        {
            itemHolder.priority.setText("REDU");
            itemHolder.priority.setTextColor(Color.parseColor("#0000FF"));
        }
        else {
            itemHolder.priority.setText("High");
            itemHolder.priority.setTextColor(Color.parseColor("#FF0000"));
        }
        /*IF Redo = 'y' or Executive_Code 'C' or Executive_Code='R' or Specific_Instruction ='R'
 (if case pending and same application exit)
 Type = 'Redu'
else
 type = 'High'
endif*/

    }
    private void forCheckingBVROrRVRData(ItemViewHolder itemHolder, DataForSecondActivity.DataBean user) {
        if(checktype.equals("BVR"))
        {
            itemHolder.department1.setVisibility(View.VISIBLE);
            itemHolder.designation1.setVisibility(View.VISIBLE);
            itemHolder.extension1.setVisibility(View.VISIBLE);

            itemHolder.department.setVisibility(View.VISIBLE);
            itemHolder.designation.setVisibility(View.VISIBLE);
            itemHolder.extension.setVisibility(View.VISIBLE);

            itemHolder.department.setText(user.getDepartment());
            itemHolder.designation.setText(user.getDesignation());
            itemHolder.extension.setText(user.getExtn());
        }

        //for RVR Type
        else {
            itemHolder.department1.setVisibility(View.GONE);
            itemHolder.designation1.setVisibility(View.GONE);
            itemHolder.extension1.setVisibility(View.GONE);

            itemHolder.department.setVisibility(View.GONE);
            itemHolder.designation.setVisibility(View.GONE);
            itemHolder.extension.setVisibility(View.GONE);

        }
    }


    public void setFilter(List<DataForSecondActivity.DataBean> filteredModelList) {
        userList = new ArrayList<>();
        userList.addAll(filteredModelList);
        notifyDataSetChanged();
    }
    public void setFilterfordate(List<DataForSecondActivity.DataBean> filteredModelListfordate) {
        userList = new ArrayList<>();
        userList.addAll(filteredModelListfordate);
        notifyDataSetChanged();
    }
    public void setFilterfordPriority(List<DataForSecondActivity.DataBean> filteredModelListforpriority) {
        userList = new ArrayList<>();
        userList.addAll(filteredModelListforpriority);
        notifyDataSetChanged();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView  date,aging, priority,appno,type, name, pinno
                ,executivecode,address,landmark,viewimages
                ,department,designation,extension
                ,department1,designation1,extension1,remark,instruction,portfolio,txtProduct;

        AppCompatButton buttonforform;
        AppCompatButton buttonforupdate;

        AppCompatButton buttonforupload ,buttonforcomment;

        public ItemViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.txtDate);
            aging= (TextView) itemView.findViewById(R.id.txtAging);
            priority = (TextView) itemView.findViewById(R.id.txtPriority);
            appno=(TextView)itemView.findViewById(R.id.txtAppId);
            type=(TextView)itemView.findViewById(R.id.txtType);
            name = (TextView) itemView.findViewById(R.id.txtName);
            executivecode = (TextView) itemView.findViewById(R.id.txtexecutivecode);
            address=(TextView)itemView.findViewById(R.id.txtAddress);
            pinno=(TextView)itemView.findViewById(R.id.txtpinno);
            landmark=(TextView)itemView.findViewById(R.id.txtlandmark);
            department1=(TextView)itemView.findViewById(R.id.department);
            designation1=(TextView)itemView.findViewById(R.id.designation);
            extension1=(TextView)itemView.findViewById(R.id.extension);
            department=(TextView)itemView.findViewById(R.id.txtdepartment);
            designation=(TextView)itemView.findViewById(R.id.txtdesignation);
            extension=(TextView)itemView.findViewById(R.id.txtextension);
            portfolio=(TextView)itemView.findViewById(R.id.txtforPortfolio);
            instruction=(TextView)itemView.findViewById(R.id.txtinstruction);

            remark = (TextView) itemView.findViewById(R.id.txtremark);
            txtProduct = (TextView) itemView.findViewById(R.id.txtProduct);

            buttonforupload = (AppCompatButton) itemView.findViewById(R.id.buttonforupload);
            buttonforcomment=(AppCompatButton) itemView.findViewById(R.id.buttonforcomment);
            viewimages=(TextView) itemView.findViewById(R.id.viewimages);
            buttonforform=(AppCompatButton)itemView.findViewById(R.id.buttonforform);
            buttonforupdate=(AppCompatButton)itemView.findViewById(R.id.buttonforupdate);

        }

    }

}
