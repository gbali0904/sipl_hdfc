package com.Survin_India_Pvt_LTD.app.myapplication.Model;

import java.util.List;

/**
 * Created by Gunjan on 02-02-2017.
 */

public class DataForLogin {

    /**
     * status : True
     * message : Login Successfull
     * user_data : {"VerifierId":50,"Name":"OTHERS","Mobile":"9711507090","password":"9875","status":"1"}
     * Portfolio : ["CARD","ME","AUTOPL"]
     */

    private String status;
    private String message;
    private UserDataBean user_data;
    private List<String> Portfolio;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDataBean getUser_data() {
        return user_data;
    }

    public void setUser_data(UserDataBean user_data) {
        this.user_data = user_data;
    }

    public List<String> getPortfolio() {
        return Portfolio;
    }

    public void setPortfolio(List<String> Portfolio) {
        this.Portfolio = Portfolio;
    }

    public static class UserDataBean {
        /**
         * VerifierId : 50
         * Name : OTHERS
         * Mobile : 9711507090
         * password : 9875
         * status : 1
         */

        private int VerifierId;
        private String Name;
        private String Mobile;
        private String password;
        private String status;

        public int getVerifierId() {
            return VerifierId;
        }

        public void setVerifierId(int VerifierId) {
            this.VerifierId = VerifierId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
