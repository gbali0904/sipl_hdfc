package com.Survin_India_Pvt_LTD.app.myapplication.service;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.Survin_India_Pvt_LTD.app.myapplication.Model.JsonClassForFragment;
import com.Survin_India_Pvt_LTD.app.myapplication.Model.User;
import com.Survin_India_Pvt_LTD.app.myapplication.activity.SecondActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.application.App;
import com.Survin_India_Pvt_LTD.app.myapplication.utills.TAGS;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.URL;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;


/**
 * Created by Gunjan on 11-09-2016.
 */
public class MyService extends Service implements LocationListener {
    public static final String BASE_URL = "http://survinindia.com/SISPL/APP_JSON/hdfc/track_verifier.php?";
    private static final String TAG = "Myservice Activity";
    private static final long MIN_DISTANCE = 1; // 1 meter
    private static final long TIME_DELAY = 300000; // 1 meter
    private static long MIN_TIME = 1 * 5 * 1000; // 5 seconds
    User user = new User();
    LocationManager locationManager;
    String mprovider;
    double LONGITUDE;
    double LATITUDE;
    String address;
    String Map;
    // flag for GPS status
    boolean isGPSEnabled = false, isNetworkEnabled = false;
    String adminId, userId;
    int battery;
    private SecondActivity context;
    private Intent mIntent;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //   Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        mIntent = intent;
        userId = intent.getStringExtra(TAGS.KEY_USERID);
        getLocationAddress();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startid) {
        userId = intent.getStringExtra(TAGS.KEY_USERID);
      /*  final String stringExtra = intent.getStringExtra(TAGS.JSONDATA);
        App.getInstance().getPreferencesEditor().putString(TAGS.JSON_FORM_DATA,stringExtra).commit();
        Toast.makeText(getApplicationContext(),"onstart"+ userId + "\n" + adminId, Toast.LENGTH_LONG).show();
        final String jsonFormData = App.getInstance().getGSON().toJson(stringExtra);
        final String string = App.getInstance().getPreferences().getString(TAGS.JSON_FORM_DATA, jsonFormData);*/

        // Log.e(TAG, "onstart"+ userId );
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag");
        wl.acquire();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                getLocationAddress();
//                getLocationTest();
                handler.postDelayed(this, TIME_DELAY);
            }

        }, TIME_DELAY);
        //     this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    //for uploadingaddress
    private boolean getLocationAddress() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);//true if required
        criteria.setBearingRequired(false);//true if required
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        mprovider = locationManager.getBestProvider(criteria, false);
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {

            buildAlertMessageNoGps();
            // no network provider is enabled
            //  Toast.makeText(getApplicationContext(), "GPS IS OFF", Toast.LENGTH_LONG).show();
        } else {
            ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

            if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
                Log.e(TAG, "no internet connection");
                //   Toast.makeText(context, "No Internet connection!", Toast.LENGTH_LONG).show();
                return false;
            } else {
                if (mprovider != null && !mprovider.equals("")) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return true;
                    }
                    Location location = locationManager.getLastKnownLocation(mprovider);
                    if (location == null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                    locationManager.requestLocationUpdates(mprovider, 15000, 1, this);
                    if (location != null) {
                        onLocationChanged(location);
                        Map = user.getLocationAddress();
                        IntentFilter mIntentFilter = new IntentFilter();
                        mIntentFilter.addAction(Intent.ACTION_BATTERY_LOW);
                        mIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
                        mIntentFilter.addAction(Intent.ACTION_BATTERY_OKAY);
                        Intent batteryIntent = registerReceiver(null, mIntentFilter);
                        float batteryLevel = getBatteryLevel(batteryIntent);
                        battery = (int) batteryLevel;
                        trackUser(Map, userId, LONGITUDE, LATITUDE, (String.valueOf(battery)));
                        Log.e(TAG, Map);
                    } else
                        Toast.makeText(getBaseContext(), "No Location Provider Found Check Your Code", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {

        LONGITUDE = location.getLongitude();
        LATITUDE = location.getLatitude();
        LocationActivity timeSpan = new LocationActivity();
        address = timeSpan.addressfromLatLon(this, LONGITUDE, LATITUDE);
        user.setLocationAddress(address);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onDestroy() {
        System.out.println("Service Destroyed");
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        // TODO Auto-generated method stub
        System.out.println("onTaskRemoved");
        super.onTaskRemoved(rootIntent);

    }

    private void trackUser(String Finaladdress, String userId, double LONGITUDE, double LATITUDE, String battery) {
        String UrlgetFormDataURL = "" + BASE_URL
                + "verifier_id=" + userId
                + "&latitude=" + LATITUDE
                + "&longitude=" + LONGITUDE
                + "&verfier_location=" + battery
                + "&ver_address=" + Finaladdress;
        UrlgetFormDataURL.trim();
        URL url;
        url = API.convertToUrl(UrlgetFormDataURL);
        Log.d(TAG, "UrlgetFormDataURL url: " + url.toString());
        if (userId.equalsIgnoreCase("") && LATITUDE == 0 && LONGITUDE == 0 && battery.equalsIgnoreCase(" ") && Finaladdress.equalsIgnoreCase(" ")) {
            Toast.makeText(getApplicationContext(), "Please Check Your Network", Toast.LENGTH_LONG).show();

        } else {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url.toString(), null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //  Toast.makeText(getContext(),  response.toString() , Toast.LENGTH_LONG).show();
                    Gson gson = new Gson();
                    JsonClassForFragment allMainUser = gson.fromJson(response.toString(), JsonClassForFragment.class);
                    final List<String> status = allMainUser.getStatus();
                    String message = null;
                    // retrieving data from string list array in for loop
                    for (int i = 0; i < status.size(); i++) {
                        Log.i("Value of element " + i, status.get(i));
                        message = status.get(i);
                    }
                    if (message.equalsIgnoreCase("success")) {
                        //   Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Log.d(TAG, message);

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                    Log.d(TAG, "error: " + error.toString());
                    Toast.makeText(getApplicationContext(), "--" + "Error" + error.toString() + "!", Toast.LENGTH_LONG).show();
                }
            });
            App.getInstance().addToRequestQueue(request);
        }
    }

    public float getBatteryLevel(Intent batteryIntent) {
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        if (level == -1 || scale == -1) {
            return 50.0f;
        }
        return ((float) level / (float) scale) * 100.0f;
    }

    private void buildAlertMessageNoGps() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                    }
                }).create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.show();

    }

    private void getLocationTest() {
        SmartLocation.with(MyService.this).location().start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                SmartLocation.with(MyService.this).geocoding().reverse(location, new OnReverseGeocodingListener() {
                    @Override
                    public void onAddressResolved(Location location, List<Address> list) {
                        if (list.size() > 0) {
                            String locationString = list.get(0).getSubLocality() + " " + list.get(0).getLocality() + " " + list.get(0).getAdminArea();
                            Log.d(TAG, "location :" + locationString);
                            if (TextUtils.isEmpty(locationString)) {//if current location is empty then use location in preference
                                String preferenceLocation = App.getInstance().getPreference().getString(TAGS.GPS_LOCATION, "");
                                if (!TextUtils.isEmpty(preferenceLocation)) {
//                                    profileLocation.setText(""+preferenceLocation);
                                }
                            } else {// if location is present then update it in preference
//                                Calundr.getInstance().getPreferenceEditor().putString(TAGS.GPS_LOCATION,locationString).commit();
//                                profileLocation.setText(""+locationString);
                            }

                        } else {// if unable to fetch location then show location from preference
                            String preferenceLocation = App.getInstance().getPreference().getString(TAGS.GPS_LOCATION, "");
                            if (!TextUtils.isEmpty(preferenceLocation)) {
//                                profileLocation.setText(""+preferenceLocation);
                            }
                        }
                    }
                });
            }
        });
    }
}

