package com.Survin_India_Pvt_LTD.app.myapplication.utills;

import com.Survin_India_Pvt_LTD.app.myapplication.Model.UploadImageModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Gunjan on 30-05-2017.
 */
public interface RequestTesting {
    @Multipart
    @POST("uploadImage.php")
    Call<UploadImageModel> postImage(@Part MultipartBody.Part image, @Part("name") RequestBody name, @Part("date") RequestBody date, @Part("map") RequestBody map);
}