package com.Survin_India_Pvt_LTD.app.myapplication.application;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.WindowManager;

import com.Survin_India_Pvt_LTD.app.myapplication.utills.ConnectivityReceiver;
import com.Survin_India_Pvt_LTD.app.myapplication.utills.LruBitmapCache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

public class App extends Application {

    public static final String TAG = App.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static App mInstance;

    private static Context mContext;
    private SharedPreferences pref;
    private Gson gson;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }



    public static synchronized App getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }
    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context mContext) {
        App.mContext = mContext;
    }




    /*
    * ------------------------------Preference related methods---------------------
	* */

    /**
     * This method is used to get instance of Default Shared preference which
     * can be used throughout the application.
     *
     * @return singleton instance of default shared preference
     */
    public SharedPreferences getPreference() {
        if (pref == null)
            pref = PreferenceManager.getDefaultSharedPreferences(App.this);
        return pref;
    }

    /**
     * This method is used to get instance of Default Shared preference editor
     * which can be used throughout the application.
     *
     * @return singleton instance of default shared preference eidtor
     */
    public SharedPreferences.Editor getPreferenceEditor() {
        if (pref == null)
            pref = PreferenceManager.getDefaultSharedPreferences(App.this);
        return pref.edit();
    }
    /*
    * ------------------------------Preference related methods---------------------
	* */


    public void checkConnectivityForInternet(Context context){
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context)
                .setMessage("No Internet Connection")
                .setCancelable(false)
                .setPositiveButton("Go To Setting", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent dialogIntent = new Intent(Settings.ACTION_SETTINGS );
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);

                    }
                })
                .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.show();
    }


    public Gson getGSON(){
        if(gson==null)
            gson=new Gson();
        return gson;
    }


}
