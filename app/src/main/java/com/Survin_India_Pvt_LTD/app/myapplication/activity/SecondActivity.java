package com.Survin_India_Pvt_LTD.app.myapplication.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import com.Survin_India_Pvt_LTD.app.myapplication.Model.DataForLogin;
import com.Survin_India_Pvt_LTD.app.myapplication.Model.DataForSecondActivity;
import com.Survin_India_Pvt_LTD.app.myapplication.R;
import com.Survin_India_Pvt_LTD.app.myapplication.adapter.RecyclerViewAdapter;
import com.Survin_India_Pvt_LTD.app.myapplication.application.App;
import com.Survin_India_Pvt_LTD.app.myapplication.service.MyService;
import com.Survin_India_Pvt_LTD.app.myapplication.utills.ConnectivityReceiver;
import com.Survin_India_Pvt_LTD.app.myapplication.utills.TAGS;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by App on 4/15/2016.
 */
public class SecondActivity extends AppCompatActivity implements SearchView.OnQueryTextListener , ConnectivityReceiver.ConnectivityReceiverListener   {

    private static final String TAG = SecondActivity.class.getSimpleName();
    private static Context context;
    RecyclerView rvUsers;
    TextView textView;
    public TextView textView3;
    String verifierId;
    private List<DataForSecondActivity.DataBean> userlist ;
    RecyclerViewAdapter recyclerViewAdapter;
    String date1, date2;
    Date dateStart = new Date();
    Date dateEnd = new Date();
    private String Portfolio="";
    private DatePicker simpleDatePicker;
    private TextView date;
    private DatePickerDialog datePickerDialog;
    String DateOfInitiation="";
    Spinner SpinnerForPortFolio;
    String Priority;
    Spinner spinner;
    // Initializing a String Array
    String[] plants = new String[]{
            "Priority",
            "REDU",
            "HIGH"
    };
    final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));
    private String response;
    private List<String> portfolio;
    private List<String> portfolioList=new ArrayList<>();
    private String formatedDate;
    private String finaldate;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        checkConnection();
      /*  App.getInstance().bus().toObserverable().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    //  int badgerValue = (int) o;
                    int badgerValue = Integer.valueOf((String) o);
                    Log.d(TAG, "new badger value " + badgerValue);
                    textView3.setText("Total No: " +badgerValue);
                }
            }
        });
*/
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();

        final ProgressDialog pd = new ProgressDialog(SecondActivity.this);
        pd.setMessage("please wait fetching your data");
        pd.show();
        showSnack(isConnected);

        pd.cancel();
    }

    private void showSnack(boolean isConnected) {

        if (isConnected) {
            checkConectivityForInternetMianActivity();

        } else {
            //  App.getInstance().checkConnectivityForInternet(this);
            Toast.makeText(getApplicationContext(),"No Internet Connection , Please Check",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register connection status listener
        App.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConectivityForInternetMianActivity() {
        portfolioList.clear();
        // Session class instance
        textView = (TextView) findViewById(R.id.textViewUsername);
        textView3 = (TextView) findViewById(R.id.txttotal);
        rvUsers = (RecyclerView) findViewById(R.id.rvUsers);
        //spinnerforpriority = (Spinner) findViewById(R.id.spinnerforpriority);
        spinner = (Spinner) findViewById(R.id.spinner);
        Intent intent = getIntent();
        textView.setText("User: " + intent.getStringExtra(TAGS.KEY_USERNAME));
        response = intent.getExtras().getString(TAGS.KEY_LOGIN_RESPONSE);


        Gson gson=new Gson();
        DataForLogin dataForLogin = gson.fromJson(response.toString(), DataForLogin.class);
        portfolio = dataForLogin.getPortfolio();
        portfolioList.add("Please Select Portfolio");
        portfolioList.add("All");
        for (int i=0 ;i<portfolio.size();i++)
        {
            portfolioList.add(portfolio.get(i));
        }

        //for id
        verifierId = intent.getStringExtra(TAGS.KEY_USERID);
        rvUsers.setHasFixedSize(true);
        rvUsers.setLayoutManager(new LinearLayoutManager(SecondActivity.this));

        refreshList1();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshList();
            }
        });


        UpdateActivity.setOnUpdateListener(new UpdateActivity.OnUpdateListener() {
            @Override
            public void onUpdate() {
                Toast.makeText(SecondActivity.this, "Successfully  Closed\u200E", Toast.LENGTH_SHORT).show();
                refreshList();
            }
        });

        //for portfolio
        //  radioGroupforcheckPortfolio();
        spinnerForPortfolio();
        //for date search
        datePickerForDateSerach();
        //for priorty
        forPriorty();
    }

    private void spinnerForPortfolio() {
        SpinnerForPortFolio=(Spinner) findViewById(R.id.SpinnerForPortFolio);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, portfolioList){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        adapter.setDropDownViewResource(R.layout.spinner_item);
        SpinnerForPortFolio.setAdapter(adapter);
        SpinnerForPortFolio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String postion1 = (String) parent.getItemAtPosition(position);
                if (position > 0) {
                    Portfolio = postion1;
                    refreshList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void forPriorty() {

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item,plantsList){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Priority = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                   /* Toast.makeText
                            (getApplicationContext(), "Selected : " + Priority, Toast.LENGTH_SHORT)
                            .show();*/
                    final List<DataForSecondActivity.DataBean > filteredModelList = filterforPriority(userlist, Priority);
                    Log.e("this", String.valueOf(filteredModelList));
                    if(!filteredModelList.isEmpty()) {

                        textView3.setText("Total No: " +filteredModelList.size());
                        recyclerViewAdapter.setFilterfordPriority(filteredModelList);
                    }
                    else {
                        textView3.setText("Total No: " +"0");
                        Toast.makeText(getApplicationContext(),"No Data Found", Toast.LENGTH_LONG).show();
                    }
                    //  Toast.makeText(getApplicationContext(), "Priority" + Priority, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void datePickerForDateSerach() {
        // initiate the date picker and a button
        date = (TextView) findViewById(R.id.date);
        // perform click event on edit text
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(SecondActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                DateOfInitiation = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
                                SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
                                SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");
                                Date myDate = null;
                                try {
                                    myDate = format1.parse(DateOfInitiation);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String finaldate = format2.format(myDate);
                                List<DataForSecondActivity.DataBean > filteredModelListfordate = filterfordate(userlist, finaldate);

                                if(!filteredModelListfordate.isEmpty()) {

                                    recyclerViewAdapter.setFilterfordate(filteredModelListfordate);
                                }
                                else {

                                    Toast.makeText(getApplication(),"NO DATA TO REFFRESH",Toast.LENGTH_LONG).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }
    //refreshing the fetching data
    public void refreshList() {
        if(userlist==null){
            Toast.makeText(getApplication(),"NO DATA TO REFFRESH",Toast.LENGTH_LONG).show();
            refreshList1();

            spinner.setSelection(0);
            //  date.setText("Date");
        }
        else if (!userlist.equals(" ")) {
            userlist.clear();
            refreshList1();
            //  date.setText("Date");
            spinner.setSelection(0);
            recyclerViewAdapter.notifyDataSetChanged();

        }
    }
    /*
      * Make a server request for fetching data
    */
    private void refreshList1() {
        Intent intent1 = new Intent(this, MyService.class);
        intent1.putExtra(TAGS.KEY_USERID, verifierId);
        startService(intent1);
        textView3.setText("Total No: " +"0");
        final ProgressDialog pd = new ProgressDialog(SecondActivity.this);
        pd.setMessage("please wait fetching your data");
        pd.show();
        String url = "http://survinindia.com/SISPL/APP_JSON/hdfc/cpv_sms.php/?VerifierId=";
        String finalurl = url + verifierId +"&Portfolio="+Portfolio;
        Log.e(TAG,finalurl);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, finalurl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //calling method to parse json array
                parseData(response);
                pd.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                txtSqlData.setText(error.toString());
                Log.d(TAG, "error: " + error.toString());
                pd.dismiss();
            }
        });
        App.getInstance().addToRequestQueue(request);

    }

    private void parseData(JSONObject response) {
        Gson gson = new Gson();
        DataForSecondActivity dataForSecondActivity = gson.fromJson(response.toString(), DataForSecondActivity.class);
        userlist = dataForSecondActivity.getData();
        // userlist.add(dataForSecondActivityBeen);

        // Toast.makeText(this, "data"+userlist.size(), Toast.LENGTH_SHORT).show();
        textView3.setText("Total No: " + userlist.size());

        recyclerViewAdapter = new RecyclerViewAdapter(SecondActivity.this, userlist);
        rvUsers.setAdapter(recyclerViewAdapter);
        //

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_settings was selected
            case R.id.action_logout:
                //        Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT).show();
                logout();
                break;
            default:
                break;
        }
        return true;
    }

    public void logout() {
        //   session.logoutUser();
        SplashActivity.savePreferences(TAGS.LOGIN_USER, "");
        SplashActivity.savePreferences(TAGS.KEY_MOBILENO, "");
        SplashActivity.savePreferences(TAGS.KEY_PASS, "");
        SplashActivity.savePreferences(TAGS.KEY_STATUS, "");
        SplashActivity.savePreferences(TAGS.KEY_USERNAME, "");
        SplashActivity.savePreferences(TAGS.KEY_USERID, "");
        Intent ints=new Intent(SecondActivity.this,MainActivity.class);
        startActivity(ints);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Iconify the widget
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    private List<DataForSecondActivity.DataBean> filter(List<DataForSecondActivity.DataBean> models, String query) {
        query = query.toLowerCase();
        final List<DataForSecondActivity.DataBean> filteredModelList = new ArrayList<>();
        filteredModelList.clear();
        Log.d(TAG,"querry String: "+query);
        for (DataForSecondActivity.DataBean model : models) {
            String text = model.getApplicantName();
            String id = model.getAddress().toLowerCase();
            String priority = model.getPriorityCustomer();
            String date = model.getDateOfInitiation();
            String portfolio = model.getPortfolio();
            String executivecode = model.getExecutiveCode();
            String instruction = model.getSpecific_instruction();
            String type = model.getType();

            if(text==null)
            {
                text="";

            }
            if(id==null)
            {
                id="";
            }
            if(date==null)
            {
                date="";
            }
            if(priority==null)
            {
                priority="";

            }if(portfolio==null)
            {
                portfolio="";

            }
            if(executivecode==null)
            {
                executivecode="";

            }
            if(instruction==null)
            {
                instruction="";

            }
            if(type==null)
            {
                type="";

            }
            formatedDate = getFormatedDate(date);
            String priorityText="";
            if(priority.equalsIgnoreCase("Y")||executivecode.equalsIgnoreCase("C")||executivecode.equalsIgnoreCase("R")||instruction.equalsIgnoreCase("R")){
                priorityText="redu";
            }else{
                priorityText="high";
            }
            Log.d(TAG,"priority log from model: "+priority);
            Log.d(TAG,"priority log: text "+priorityText);
            priorityText=  priorityText.toLowerCase();//.toLowerCase()
            if (text.toLowerCase().contains(query) || id.toLowerCase().contains(query) ||
                    portfolio.toLowerCase().contains(query) ||
                    priorityText.toLowerCase().contains(query)||
                    formatedDate.toLowerCase().contains(query)||
                    type.toLowerCase().contains(query)
                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
    private List<DataForSecondActivity.DataBean> filterfordate(List<DataForSecondActivity.DataBean> userlistfordate, String dateOfInitiation) {

        List<DataForSecondActivity.DataBean>  filteredModelList = new ArrayList<>();
        filteredModelList.clear();
        Log.d(TAG,"querry String: "+dateOfInitiation);
        if(dateOfInitiation!=null && userlistfordate!=null) {
            for (DataForSecondActivity.DataBean model : userlistfordate) {
                final String date = model.getDateOfInitiation().toLowerCase();
                String formatedDate = getFormatedDate(date);

                if (formatedDate.contains(dateOfInitiation)) {
                    filteredModelList.add(model);
                }
                Log.e("this","formed_data"+filteredModelList);
            }
        }
        else {
            Toast.makeText(getApplicationContext(),"No Data To Load",Toast.LENGTH_LONG).show();
        }

        return filteredModelList;
    }
    private List<DataForSecondActivity.DataBean> filterforPriority(List<DataForSecondActivity.DataBean> userlist, String priority) {
        priority=priority.toLowerCase();
        final List<DataForSecondActivity.DataBean> filteredModelList = new ArrayList<>();
        filteredModelList.clear();
        Log.d(TAG,"querry String: "+priority);

        if(userlist!=null) {
            for (DataForSecondActivity.DataBean model : userlist) {
                String priorityfrommodel = model.getRedo().toLowerCase();
                String executivecode = model.getExecutiveCode().toLowerCase();
                String instruction = model.getSpecific_instruction().toLowerCase();
                String priorityText = "";

                if (priorityfrommodel == null) {
                    priorityfrommodel = "";

                }
                if (executivecode == null) {
                    executivecode = "";

                }
                if (instruction == null)

                {
                    instruction = "";

                }
                if (priorityfrommodel.equalsIgnoreCase("Y") || executivecode.equalsIgnoreCase("C") || executivecode.equalsIgnoreCase("R") || instruction.equalsIgnoreCase("R")) {
                    priorityText = "redu";
                } else {
                    priorityText = "high";
                }
                if (priorityText.contains(priority)) {
                    filteredModelList.add(model);
                }

            }
        }
        else {
            Toast.makeText(getApplicationContext(),"No Data To Load",Toast.LENGTH_LONG).show();
        }
        return filteredModelList;

    }

    private String getFormatedDate(String dateString) {


        //2017-05-30
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");

        try {
            Date date = format1.parse(dateString);
            finaldate = format2.format(date);
            Log.e("this date string",dateString+"\n final date"+finaldate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finaldate;
    }
    @Override
    public boolean onQueryTextChange(String query) {
        boolean flag ;
        if(userlist!=null) {
            final List<DataForSecondActivity.DataBean> filteredModelList = filter(userlist, query);
            recyclerViewAdapter.setFilter(filteredModelList);
            flag=true;
        }
        else {
            Toast.makeText(getApplicationContext(),"No Data to search",Toast.LENGTH_LONG).show();
            flag =false;
        }
        return flag;
    }

}

