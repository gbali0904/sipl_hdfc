package com.Survin_India_Pvt_LTD.app.myapplication.utills;

/**
 * Created by sh on 6/23/16.
 */
public class Events {
    private Events(){}

    public static class Message {
        public final int message;

        public Message(int message) {
            this.message = message;
        }
    }
}
