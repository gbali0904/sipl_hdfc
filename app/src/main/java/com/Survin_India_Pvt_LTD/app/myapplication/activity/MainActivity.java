package com.Survin_India_Pvt_LTD.app.myapplication.activity;

        import android.app.AlertDialog;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.location.LocationManager;
        import android.net.ConnectivityManager;
        import android.os.Bundle;

        import android.support.v7.app.AppCompatActivity;
        import android.support.v7.widget.AppCompatButton;
        import android.util.Log;
        import android.view.View;
        import android.view.Window;
        import android.view.WindowManager;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.LinearLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.Survin_India_Pvt_LTD.app.myapplication.Model.DataForLogin;
        import com.Survin_India_Pvt_LTD.app.myapplication.R;
        import com.Survin_India_Pvt_LTD.app.myapplication.application.App;
        import com.Survin_India_Pvt_LTD.app.myapplication.utills.TAGS;
        import com.android.volley.Request;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.JsonObjectRequest;
        import com.google.gson.Gson;

        import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText editTextforuser;
    EditText editTextforpassword;
    Button buttonforlogin;
    LinearLayout linearLayout;
    private static final String TAG = MainActivity.class.getSimpleName();
  //  public static final String LOGIN_URL = "http://survinindia.in/HDFCAPP/APP_Json/new-json/login.php/?Mobile=";
    public static final String LOGIN_URL = "http://survinindia.com/SISPL/APP_JSON/hdfc/login.php/?Mobile=";
    private String username;
    private String password;
    TextView forgotpassword;
    private String name = "";
    private String verifierId = "";
    private String mobileno = "",mobileno1="";
    private String pass = "";
    String status = "";
    private String checkStatus = "1";
    // Session Manager Class
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_activity);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // getting GPS status
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            buildAlertMessageNoGps();
            // no network provider is enabled
            //  Toast.makeText(getApplicationContext(), "GPS IS OFF", Toast.LENGTH_LONG).show();
        }
        else {
            checkConnectivity();
        }
    }

    private void buildAlertMessageNoGps() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                    }
                }).create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.show();

    }

    private void checkConnectivity() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)&& (wifi.isConnected() | datac.isConnected())) {
            editTextforuser = (EditText) findViewById(R.id.editTextforuser);
            editTextforpassword = (EditText) findViewById(R.id.editTextforpassword);
            buttonforlogin = (Button) findViewById(R.id.btn_signup);
            buttonforlogin.setOnClickListener(this);
            forgotpassword=(TextView) findViewById(R.id.textforgotpassword);
                }
        else {
            //no connection
            Toast toast = Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_LONG);
            toast.show();
            showNetDisabledAlertToUser(MainActivity.this);
        }


    }

    public static void showNetDisabledAlertToUser(final Context context){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setMessage("Would you like to enable it?")
                .setTitle("No Internet Connection")
                .setPositiveButton(" Enable Internet ", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(dialogIntent);
                    }
                });
        alertDialogBuilder.setNegativeButton(" Cancel ", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    private void userLogin() {
        String postvalue = username + "&password=" + password;
        String finalurl = LOGIN_URL + postvalue;

        Log.e(TAG,"finalurl"+finalurl);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, finalurl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                username = editTextforuser.getText().toString().trim();
                password = editTextforpassword.getText().toString().trim();

                Gson gson=new Gson();
                DataForLogin dataForLogin = gson.fromJson(response.toString(), DataForLogin.class);

                Log.e(TAG, "Iformation" + response.toString());
                if(dataForLogin.getMessage().equalsIgnoreCase("Login Successfull")) {
                    DataForLogin.UserDataBean user_data = dataForLogin.getUser_data();
                    mobileno = user_data.getMobile();
                    name = user_data.getName();
                    verifierId = String.valueOf(user_data.getVerifierId());
                    pass = user_data.getPassword();
                    status = user_data.getStatus();
                    if (username.equals(mobileno) && password.equals(pass) && checkStatus.equals(status)) {
                        openProfile(response);
                    } else if (!username.equals(mobileno) && !password.equals(pass) && !checkStatus.equals(status)) {
                        Toast.makeText(MainActivity.this, "Wrong Credentials", Toast.LENGTH_LONG).show();
                    } else if (!checkStatus.equals(status)) {
                        Toast.makeText(MainActivity.this, "Please check ur Status", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(MainActivity.this, dataForLogin.getMessage(), Toast.LENGTH_LONG).show();
                }
           }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "error: " + error.toString());
                Toast.makeText(MainActivity.this, "--" + "Error"+  error.toString() + "!", Toast.LENGTH_LONG).show();
            }
        });
        App.getInstance().addToRequestQueue(request);

    }
    private void openProfile(JSONObject response) {
        //    session.createLoginSession(mobileno,pass,status);

        SplashActivity.savePreferences(TAGS.LOGIN_USER,"1");
        SplashActivity.savePreferences(TAGS.KEY_MOBILENO,mobileno);
        SplashActivity.savePreferences(TAGS.KEY_PASS,pass);
        SplashActivity.savePreferences(TAGS.KEY_STATUS,status);
        SplashActivity.savePreferences(TAGS.KEY_USERNAME,name);
        SplashActivity.savePreferences(TAGS.KEY_USERID,verifierId);
        SplashActivity.savePreferences(TAGS.KEY_LOGIN_RESPONSE, String.valueOf(response));
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(TAGS.KEY_USERNAME, name);
        intent.putExtra(TAGS.KEY_USERID, verifierId);
        intent.putExtra(TAGS.KEY_MOBILENO, mobileno);
        intent.putExtra(TAGS.KEY_LOGIN_RESPONSE, String.valueOf(response));
        startActivity(intent);
        finish();
    }
    @Override
    public void onClick(View v) {
        username = editTextforuser.getText().toString().trim();
        password = editTextforpassword.getText().toString().trim();
        if ((username.equals("")) && (password.equals(""))) {
            Toast.makeText(MainActivity.this, "--" + "Please Enter UserName & Password" + "!", Toast.LENGTH_LONG).show();

        }
        else if((password.equals("")))
        {
            Toast.makeText(MainActivity.this, "--" + "Please Password" + "!", Toast.LENGTH_LONG).show();
        }
        else
        {
            userLogin();
        }
    }
}

