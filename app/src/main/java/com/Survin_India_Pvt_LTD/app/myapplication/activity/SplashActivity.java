package com.Survin_India_Pvt_LTD.app.myapplication.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;
import android.widget.TextView;

import com.Survin_India_Pvt_LTD.app.myapplication.R;
import com.Survin_India_Pvt_LTD.app.myapplication.application.App;
import com.Survin_India_Pvt_LTD.app.myapplication.utills.ConnectivityReceiver;
import com.Survin_India_Pvt_LTD.app.myapplication.utills.TAGS;

import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings("deprecation")
public class SplashActivity extends ActionBarActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    static SharedPreferences sharedPreferences;
    private static int Splash_TIME_DELEY = 3000;
    TextView tv_logo, tv_sublogo;

    public static void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPreferences(String key, String val) {
        String value = sharedPreferences.getString(key, val);
        return value;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.content_main);
        checkConnection();
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        if (isConnected) {
            splashMethodToCheckConectivity();
        } else {
            App.getInstance().checkConnectivityForInternet(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register connection status listener
        App.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void splashMethodToCheckConectivity() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (!SplashActivity.getPreferences(TAGS.LOGIN_USER, "").equalsIgnoreCase("")) {
                    String verifierId = SplashActivity.getPreferences(TAGS.KEY_USERID, "");
                    String name = SplashActivity.getPreferences(TAGS.KEY_USERNAME, "");
                    String mobileno = SplashActivity.getPreferences(TAGS.KEY_MOBILENO, "");
                    String response = SplashActivity.getPreferences(TAGS.KEY_LOGIN_RESPONSE, "");
                    Intent i = new Intent(SplashActivity.this, SecondActivity.class);
                    i.putExtra(TAGS.KEY_USERNAME, name);
                    i.putExtra(TAGS.KEY_USERID, verifierId);
                    i.putExtra(TAGS.KEY_MOBILENO, mobileno);
                    i.putExtra(TAGS.KEY_LOGIN_RESPONSE, response);
                    // UtilClassForValidations.runService(Splash_Activity.this);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    // UtilClassForValidations.runService(Splash_Activity.this);
                    startActivity(i);
                    finish();
                }

            }
        };
        Timer t = new Timer();
        t.schedule(task, Splash_TIME_DELEY);
    }
}
