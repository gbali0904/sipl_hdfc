package com.Survin_India_Pvt_LTD.app.myapplication.service;
import java.net.URI;
import java.net.URL;

/**
 * Created by Gunjan on 29-10-2016.
 */

public class API {
    public static final String BASE_URL="http://survinindia.com/HDFC/json/track_verifier.php?";
    private static final String TAG = API.class.getSimpleName();
    private static String s1;

    public static URL convertToUrl(String urlStr) {
        try {
            URL url = new URL(urlStr);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(),
                    url.getHost(), url.getPort(), url.getPath(),
                    url.getQuery(), url.getRef());
            url = uri.toURL();
            return url;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}