package com.Survin_India_Pvt_LTD.app.myapplication;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Gunjan on 30-05-2017.
 */
interface RequestTesting {
    @Multipart
    @POST("uploadImage.php")
    Call<UploadImageModel> postImage(@Part MultipartBody.Part image, @Part("name") RequestBody name, @Part("date") RequestBody date, @Part("map") RequestBody map);
}