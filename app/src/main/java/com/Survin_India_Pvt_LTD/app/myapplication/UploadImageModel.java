package com.Survin_India_Pvt_LTD.app.myapplication;

/**
 * Created by Gunjan on 30-05-2017.
 */

public class UploadImageModel {


    /**
     * error : True
     * message : Photo Uploaded Successfully.
     */

    private String error;
    private String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
