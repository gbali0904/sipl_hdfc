package com.Survin_India_Pvt_LTD.app.myapplication.Model;

import java.util.List;

/**
 * Created by Gunjan on 10-11-2016.
 */

public class UpdateResponse {

    private List<String> Status;

    public List<String> getStatus() {
        return Status;
    }

    public void setStatus(List<String> Status) {
        this.Status = Status;
    }
}
