package com.Survin_India_Pvt_LTD.app.myapplication.Model;

import java.util.List;

/**
 * Created by Gunjan on 03-02-2017.
 */

public class DataForSecondActivity {



    private List<DataBean> Data;

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * CPVId : 67659
         * ApplicationReferenceNumber : 15040615140280N1
         * ApplicantName : KASHIKA DHAWAN
         * PriorityCustomer : N
         * ExecutiveCode : ss
         * DateOfInitiation : 2/5/2017
         * Address : PEPSICO INDIA HOLDINGS PVT LTD, 1ST FLR TOWER-A BUILDING NO-8, CYBER CITY DLF PHASE-II, GURGAON
         * Landmark : ss
         * Department : R AND D
         * Designation : R AND D ASSISTANT MANAGER
         * Extn : ss
         * Type : BVR
         * AssignedTo : 50
         * CurrentStatus : 0
         * Comment : ss
         * lastsms : 2017-02-13 12:54:48
         * VerifyByMobile : 0
         * VerifierClosedOn : ss
         * REMARKS : ss
         * Specific_instruction : ss
         * Priority : ss
         * Redo : ss
         * Portfolio : AUTOPL
         * buttonverified : ss
         * PinCode : 122002
         */

        private int CPVId;
        private String ApplicationReferenceNumber;
        private String ApplicantName;
        private String PriorityCustomer;
        private String ExecutiveCode;
        private String DateOfInitiation;
        private String Address;
        private String Landmark;
        private String Department;
        private String Designation;
        private String Extn;
        private String Type;
        private int AssignedTo;
        private int CurrentStatus;
        private String Comment;
        private String lastsms;
        private int VerifyByMobile;
        private String VerifierClosedOn;
        private String REMARKS;
        private String Specific_instruction;
        private String Priority;
        private String Redo;
        private String Portfolio;
        private String buttonverified;
        private String PinCode;


        private String Product;

        private String date;
        private String finalaging;
        private String aging;
        private String namevisibility;

        public int getCPVId() {
            return CPVId;
        }

        public void setCPVId(int CPVId) {
            this.CPVId = CPVId;
        }

        public String getApplicationReferenceNumber() {
            return ApplicationReferenceNumber;
        }

        public void setApplicationReferenceNumber(String ApplicationReferenceNumber) {
            this.ApplicationReferenceNumber = ApplicationReferenceNumber;
        }

        public String getApplicantName() {
            return ApplicantName;
        }

        public void setApplicantName(String ApplicantName) {
            this.ApplicantName = ApplicantName;
        }

        public String getPriorityCustomer() {
            return PriorityCustomer;
        }

        public void setPriorityCustomer(String PriorityCustomer) {
            this.PriorityCustomer = PriorityCustomer;
        }

        public String getExecutiveCode() {
            return ExecutiveCode;
        }

        public void setExecutiveCode(String ExecutiveCode) {
            this.ExecutiveCode = ExecutiveCode;
        }

        public String getDateOfInitiation() {
            return DateOfInitiation;
        }

        public void setDateOfInitiation(String DateOfInitiation) {
            this.DateOfInitiation = DateOfInitiation;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getLandmark() {
            return Landmark;
        }

        public void setLandmark(String Landmark) {
            this.Landmark = Landmark;
        }

        public String getDepartment() {
            return Department;
        }

        public void setDepartment(String Department) {
            this.Department = Department;
        }

        public String getDesignation() {
            return Designation;
        }

        public void setDesignation(String Designation) {
            this.Designation = Designation;
        }

        public String getExtn() {
            return Extn;
        }

        public void setExtn(String Extn) {
            this.Extn = Extn;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public int getAssignedTo() {
            return AssignedTo;
        }

        public void setAssignedTo(int AssignedTo) {
            this.AssignedTo = AssignedTo;
        }

        public int getCurrentStatus() {
            return CurrentStatus;
        }

        public void setCurrentStatus(int CurrentStatus) {
            this.CurrentStatus = CurrentStatus;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String Comment) {
            this.Comment = Comment;
        }

        public String getLastsms() {
            return lastsms;
        }

        public void setLastsms(String lastsms) {
            this.lastsms = lastsms;
        }

        public int getVerifyByMobile() {
            return VerifyByMobile;
        }

        public void setVerifyByMobile(int VerifyByMobile) {
            this.VerifyByMobile = VerifyByMobile;
        }

        public String getVerifierClosedOn() {
            return VerifierClosedOn;
        }

        public void setVerifierClosedOn(String VerifierClosedOn) {
            this.VerifierClosedOn = VerifierClosedOn;
        }

        public String getREMARKS() {
            return REMARKS;
        }

        public void setREMARKS(String REMARKS) {
            this.REMARKS = REMARKS;
        }

        public String getSpecific_instruction() {
            return Specific_instruction;
        }

        public void setSpecific_instruction(String Specific_instruction) {
            this.Specific_instruction = Specific_instruction;
        }

        public String getPriority() {
            return Priority;
        }

        public void setPriority(String Priority) {
            this.Priority = Priority;
        }

        public String getRedo() {
            return Redo;
        }

        public void setRedo(String Redo) {
            this.Redo = Redo;
        }

        public String getPortfolio() {
            return Portfolio;
        }

        public void setPortfolio(String Portfolio) {
            this.Portfolio = Portfolio;
        }

        public String getButtonverified() {
            return buttonverified;
        }

        public void setButtonverified(String buttonverified) {
            this.buttonverified = buttonverified;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }
        public String getProduct() {
            return Product;
        }

        public void setProduct(String Product) {
            this.Product = Product;
        }


        //for date
        public String getdate() {
            return date;
        }

        public void setdate(String date) {
            this.date = date;
        }

        //for finalaging
        public String getfinalaging() {
            return finalaging;
        }

        public void setfinalaging(String finalaging) {
            this.finalaging = finalaging;
        }

        //for aging
        public String getaging() {
            return aging;
        }

        public void setaging(String aging) {
            this.aging = aging;
        }
    }
}